export class Student {
    id: number;
    nombre: string;
    apellido:string;
    createAt:string;
    email: string;
    role: boolean;
}
