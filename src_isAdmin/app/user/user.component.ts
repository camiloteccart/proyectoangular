import { Component, OnInit } from '@angular/core';
import { Student } from '../students/student';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html'
})
export class UserComponent implements OnInit {

  constructor() { }

  students: Student[] = [
  {id:1, nombre: 'Andres1', apellido: 'Guzman1', createAt: '2017-11-20', email: 'correo1@hotmail.com', role: true},
  {id:2, nombre: 'Andres2', apellido: 'Guzman2', createAt: '2017-11-20', email: 'correo2@hotmail.com', role: false},
  {id:3, nombre: 'Andres3', apellido: 'Guzman3', createAt: '2017-11-20', email: 'correo3@hotmail.com', role: true},
  {id:4, nombre: 'Andres4', apellido: 'Guzman4', createAt: '2017-11-20', email: 'correo4@hotmail.com', role: false},
  {id:5, nombre: 'Andres5', apellido: 'Guzman5', createAt: '2017-11-20', email: 'correo5@hotmail.com', role: true}];

  ngOnInit(): void {
  }

}
