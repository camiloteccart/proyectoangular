import { Component, OnInit } from '@angular/core';
import { Student } from './student';
import { StudentService } from './student.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html'
})
export class StudentsComponent implements OnInit {

  students: Student[];
  constructor(private studentService: StudentService) { }

  ngOnInit() {
    this.studentService.getStudents().subscribe(
      students => this.students = students
    );
  }

  delete(student: Student): void {
    swal.fire({
      title: 'Está seguro?',
        text: `¿Seguro que desea eliminar al estudiante ${student.nombre} ${student.apellido}?`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText: 'No, cancelar!'
    }).then((result) => {
      if (result.value) {
        this.studentService.delete(student.id).subscribe(
          response => {
            this.students = this.students.filter(stu => stu !== student)
            swal.fire(
              'Estudiante Eliminado!',
              `Estudiante ${student.nombre} eliminado con éxito.`,
              'success'
            )
          }
        )

      }
    })
  }

}
