import { Injectable } from '@angular/core';
import {STUDENTS} from './students.json';
import {Student} from './student';
import { of, Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import swal from 'sweetalert2'
import {Router, ActivatedRoute} from '@angular/router'

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  private urlEndPoint: string = 'http://localhost:8080/api/alumnos';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})

  constructor(private http: HttpClient, private router: Router) { }

  getStudents(): Observable<Student[]>{
    //return of(STUDENTS);
    return this.http.get<Student[]>(this.urlEndPoint);
  }

  create(student: Student) : Observable<Student> {
    return this.http.post<Student>(this.urlEndPoint, student, {headers: this.httpHeaders})
  }

  getStudent(id): Observable<Student>{
    return this.http.get<Student>(`${this.urlEndPoint}/${id}`).pipe(
      catchError(e => {
        this.router.navigate(['/students'])
        console.error(e.error.mensaje)
        swal.fire('Error al editar', e.error.mensaje, 'error')
        return throwError(e)
      })
    )
  }

  update(student: Student): Observable<Student>{
    return this.http.put<Student>(`${this.urlEndPoint}/${student.id}`, student, {headers: this.httpHeaders})
  }

  delete(id: number): Observable<Student>{
    return this.http.delete<Student>(`${this.urlEndPoint}/${id}`, {headers: this.httpHeaders})
  }
}
