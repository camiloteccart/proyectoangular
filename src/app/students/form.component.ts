import { Component, OnInit } from '@angular/core';
import { Student} from './student';
import {StudentService} from './student.service'
import {Router, ActivatedRoute} from '@angular/router'
import swal from 'sweetalert2'

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

public student: Student = new Student()
public titulo:string = "Crear Estudiante"
  constructor(private studentService: StudentService, private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.cargarStudent()
  }

  cargarStudent(): void{
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if(id){
        this.studentService.getStudent(id).subscribe( (student) => this.student = student)
      }
    })
  }

  public create(): void {
    this.studentService.create(this.student)
      .subscribe(student => {
        this.router.navigate(['/students'])
        swal.fire('Nuevo estudiante', `Estudiante ${student.nombre} creado con éxito!`, 'success')
      }
      );
  }

  update():void{
    this.studentService.update(this.student)
    .subscribe( student => {
      this.router.navigate(['/students'])
      swal.fire('Estudiante Actualizado', `Estudiante ${student.nombre} actualizado con éxito!`, 'success')
    }

    )
  }

}
