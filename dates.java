@echo off
rem powershell Get-Date -date (Get-Date).ToUniversalTime()-uformat %Y%m%d%H%M
rem powershell Get-Date
rem powershell Get-Date -date (Get-Date).ToUniversalTime()
rem powershell Get-Date -date (Get-Date).ToUniversalTime() -format 'yyyy-MM-dd HH_mm'

rem @echo fecha: %datetime%
rem for /F "usebackq tokens=1,2 delims==" %%i in (`wmic os get LocalDateTime /VALUE 2^>NUL`) do if '.%%i.'=='.LocalDateTime.' set ldt=%%j
rem set ldt=%ldt:~0,4%-%ldt:~4,2%-%ldt:~6,2% %ldt:~8,2%:%ldt:~10,2%:%ldt:~12,6%
rem echo Local date is [%ldt%]

rem for /f "delims=" %%a in ('powershell Get-Date') do Set "$datetime=%%a"
rem for /f "delims=" %%a in ('powershell -Command "Get-Date -format 'yyyy-MM-dd HH_mm_ss'" ') do set "datetime=%%a"

rem GET DATE TIME IN UTC1
rem for /f "delims=" %%a in ('powershell -Command "Get-Date -date (Get-Date).ToUniversalTime() -format 'yyyy-MM-dd HH_mm_ss'" ') do set "datetime=%%a"
rem Echo Value received from Powershell : %datetime%


rem for /f %%x in ('wmic path win32_utctime get /format:list ^| findstr "="') do set %%x
rem Echo today=%Year%-%Month:~-2%-%Day% %Hour%:%Minute%

for /F "delims=" %%Z in ('wmic path win32_utctime get Day^,Month^,Year /FORMAT:LIST') do (
    for /F "tokens=1* delims==" %%X in ("%%Z") do set "%%X=0%%Y"
)
echo today=%Year:~-4%-%Month:~-2%-%Day:~-2%
