import { Student } from './student'
export const STUDENTS: Student[] = [
  {id: 1, nombre: 'Uno', apellido: 'UnoApellido', email: 'uno@hotmail.com', createAt:'2017-12-11'},
  {id: 2, nombre: 'Dos', apellido: 'DosApellido', email: 'dos@hotmail.com', createAt:'2017-12-11'},
  {id: 3, nombre: 'Tres', apellido: 'TresApellido', email: 'tres@hotmail.com', createAt:'2017-12-11'},
  {id: 4, nombre: 'Cuatro', apellido: 'CuatroApellido', email: 'cuatro@hotmail.com', createAt:'2017-12-11'},
  {id: 5, nombre: 'Cinco', apellido: 'CincoApellido', email: 'cinco@hotmail.com', createAt:'2017-12-11'}
];
